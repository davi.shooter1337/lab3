#include "functions.h"
#include <QString>

QString binToDec (QString num)
{
    QString decStr;
    bool ok;
    int dec = num.toInt(&ok,2);

    if (ok)
    {
        decStr.setNum(dec);
        return decStr;
    }
    else return nullptr;
}

QString decToBin (QString num)
{
    QString binStr;
    int dec = num.toInt();
    binStr = QString("%1").arg(dec,0,2);
    return binStr;
}

QString binToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 2);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}

QString octToBin (QString num)
{
    QString binStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        binStr = QString("%1").arg(dec,0,2);
        return binStr;
    }
    else return nullptr;
}

QString octToHex (QString num)
{
    QString hexStr;
    bool ok;
    int dec = num.toInt(&ok,8);

    if (ok)
    {
        hexStr = QString("%1").arg(dec,0,16);
        return hexStr;
    }
    else return nullptr;
}

QString hexToOct (QString num)
{
    QString octStr;
    bool ok;
    int dec = num.toInt(&ok, 16);

    if (ok)
    {
        octStr = QString("%1").arg(dec,0,8);
        return octStr;
    }
    else return nullptr;
}
