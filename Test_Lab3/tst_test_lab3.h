#ifndef TST_TEST_LAB3_H
#define TST_TEST_LAB3_H

#include <QtCore>
#include <QtTest/QtTest>


class Test_Lab3 : public QObject
{
    Q_OBJECT

public:
    Test_Lab3();

private slots:
    void test_binToDec();
    void test_decToBin();
    void test_binToOct();
    void test_octToBin();
    void test_octToHex();
    void test_hexToOct();

};

#endif // TST_TEST_LAB3_H
