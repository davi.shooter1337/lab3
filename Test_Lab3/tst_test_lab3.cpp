#include <QtTest>
#include <../functions.h>

// add necessary includes here

class Test_Lab3 : public QObject
{
    Q_OBJECT

public:
    Test_Lab3();
    ~Test_Lab3();

private slots:
    void test_binToDec();
    void test_decToBin();
    void test_binToOct();
    void test_octToBin();
    void test_octToHex();
    void test_hexToOct();

};

Test_Lab3::Test_Lab3()
{

}

void Test_Lab3::test_binToDec()
{
    QString num("1001");
    QCOMPARE("9", binToDec(num));
}

void Test_Lab3::test_decToBin()
{
    QString num("20");
    QCOMPARE("10100", decToBin(num));
}

void Test_Lab3::test_binToOct()
{
    QString num("11101");
    QCOMPARE("35", binToOct(num));
}

void Test_Lab3::test_octToBin()
{
    QString num("42");
    QCOMPARE("100010",octToBin(num));
}

void Test_Lab3::test_octToHex()
{
    QString num("74");
    QCOMPARE("3c",octToHex(num));
}

void Test_Lab3::test_hexToOct()
{
    QString num("e9");
    QCOMPARE("351",hexToOct(num));
}
